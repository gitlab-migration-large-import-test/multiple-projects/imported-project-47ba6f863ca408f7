# frozen_string_literal: true

class LogEntryLevel < Mongoid::Migration
  def self.up
    Update::LogEntry.batch_size(1000).each do |entry|
      log_level = entry.attributes["level"]
      next if log_level.nil? || log_level.is_a?(Integer)

      entry.update_attributes!(level: LogLevel.to_numeric(log_level))
    end

    Update::LogEntry.collection.indexes.create_one({ level: 1 })
  end

  def self.down; end
end
