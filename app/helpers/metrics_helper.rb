# frozen_string_literal: true

module MetricsHelper
  class << self
    # Active vulnerabilities in a project
    #
    # @param [Project] project
    # @return [Integer]
    def active_vulnerability_issues(project)
      project.vulnerability_issues.where(state: "openend").count
    end

    # Active merge requests fixing a vulnerability in a project
    #
    # @param [Project] project
    # @return [Integer]
    def active_vulnerability_merge_requests(project)
      project.merge_requests.where(state: "opened", fixes_vulnerability: true).count
    end

    # Total amount of vulnerabilities detected in a project
    #
    # @param [Project] project
    # @return [Integer]
    def total_vulnerabilities(project)
      project.vulnerability_issues.count + project.merge_requests.where(fixes_vulnerability: true).count
    end
  end
end
