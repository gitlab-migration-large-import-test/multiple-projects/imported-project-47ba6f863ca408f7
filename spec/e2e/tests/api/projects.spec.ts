import { test, expect } from "@playwright/test";
import { Smocker } from "@support/smocker";
import randomstring from "randomstring";

let smocker: Smocker;
let projectName: string;

test.beforeEach(async () => {
  projectName = randomstring.generate({ length: 10, charset: "alphabetic" });
  smocker = await new Smocker(projectName).init();

  await smocker.reset();
});

test.afterEach(async () => {
  await smocker.verify();
  await smocker.dispose();
});

test("adds new project without specific token", async ({ request }) => {
  await smocker.add([
    { name: "project" },
    { name: "hook" },
    { name: "set_hook" },
    { name: "present_config" },
    { name: "raw_config" }
  ]);

  const response = await request.post("/api/v2/projects", {
    data: { project_name: projectName }
  })
  const responseJson = await response.json();

  expect(response.ok()).toBeTruthy();
  expect(responseJson.name).toEqual(projectName);
})

test("adds new project with specific token", async ({ request }) => {
  await smocker.add([
    { name: "project" },
    { name: "hook" },
    { name: "set_hook" },
    { name: "present_config" },
    { name: "raw_config" }
  ]);

  const response = await request.post("/api/v2/projects", {
    data: { project_name: projectName, gitlab_access_token: "token" }
  })
  const responseJson = await response.json();

  expect(response.ok()).toBeTruthy();
  expect(responseJson.name).toEqual(projectName);
})
