import { test } from "@playwright/test";
import { ProjectsPage } from "@pages/projects";
import { NewProjectPage } from "@pages/newProject";
import { Smocker } from "@support/smocker";
import randomstring from "randomstring";

let smocker: Smocker;
let projectName: string;

test.beforeEach(async () => {
  projectName = randomstring.generate({ length: 10, charset: "alphabetic" });
  smocker = await new Smocker(projectName).init();

  await smocker.reset();
});

test.afterEach(async () => {
  await smocker.verify();
  await smocker.dispose();
});

test("renders main page", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);
  await projectsPage.visit();

  await projectsPage.expectPageToBeVisible();
});

test("renders new project page", async ({ page }) => {
  const newProjectPage = new NewProjectPage(page);
  await newProjectPage.visit();

  await newProjectPage.expectPageToBeVisible();
});

test("adds new project without specific token", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);
  const newProjectPage = new NewProjectPage(page);
  const mockDefinition = [
    { name: "project" },
    { name: "hook" },
    { name: "set_hook" },
    { name: "present_config" },
    { name: "raw_config" }
  ];

  await smocker.add(mockDefinition);
  await projectsPage.visit();
  await projectsPage.goToNewProject();
  await newProjectPage.addProject(projectName);

  await projectsPage.expectProjectToBeVisible(projectName);
});

test("adds new project with specific token", async ({ page }) => {
  const projectsPage = new ProjectsPage(page);
  const newProjectPage = new NewProjectPage(page);
  const mockDefinition = [
    { name: "project" },
    { name: "hook" },
    { name: "set_hook" },
    { name: "present_config" },
    { name: "raw_config" }
  ];
  const token = "token";

  await smocker.add(mockDefinition);
  await projectsPage.visit();
  await projectsPage.goToNewProject();
  await newProjectPage.addProject(projectName, token);

  await projectsPage.expectProjectToBeVisible(projectName);
});
